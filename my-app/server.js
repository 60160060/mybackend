const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.json({ message: 'Hello Pangya!!!' })
})

app.listen(9001, () => {
  console.log('Application is running on port 9001')
})
